package Main;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Random;

import javax.swing.JFrame;

import Graphics.Screen;
import Input.Keyboard;

public class Game extends Canvas implements Runnable {

	private Random random = new Random();
	private static final long serialVersionUID = 1L;

	public static int width = 300;
	public static int height = width / 16 * 9;

	public static int scale = 3;

	private Thread thread;

	private boolean running = false;

	private JFrame frame;

	private BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

	private int pixels[] = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

	private Screen screen;

	public static String title = "Game!";

	public int xOffset = 0, yOffset = 0;

	private Keyboard keyboard;

	public Game() {
		Dimension size = new Dimension(width * scale, height * scale);
		this.setPreferredSize(size);
		screen = new Screen(this.width, this.height);
		frame = new JFrame();

		keyboard = new Keyboard();
		this.addKeyListener(keyboard);
	}

	public synchronized void start() {

		running = true;

		thread = new Thread(this, "Display");

		thread.start();
	}

	public synchronized void stop() {
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void run() {

		int frames = 0, updates = 0;
		long timer = System.currentTimeMillis();
		long lastTime = System.nanoTime();
		final double ns = 1000000000.0 / 60;
		double delta = 0;

		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				update();
				updates++;
				delta--;
			}
			render();
			frames++;
			if (System.currentTimeMillis() - timer >= 1000) {
				timer += 1000;
				frame.setTitle(title + " | " + updates + " ups, " + frames + "fps");
				updates = 0;
				frames = 0;
			}
		}

	}

	public void update() {
		keyboard.update();

		if (keyboard.up == true)
			yOffset--;
		if (keyboard.down == true)
			yOffset++;
		if (keyboard.left == true)
			xOffset--;
		if (keyboard.right == true)
			xOffset++;

	}

	public void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(3);
			return;
		}

		screen.clear();
		screen.render(xOffset, yOffset);

		for (int i = 0; i < pixels.length; i++)
			pixels[i] = screen.pixels[i];

		Graphics g = bs.getDrawGraphics();

		g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), null);
		g.setColor(new Color(random.nextInt(0xffffff)));
		g.fill3DRect((this.getWidth()) / 2, (this.getHeight()) / 2, 96, 96, true);

		g.dispose();
		bs.show();
	}

	public static void main(String args[]) {
		Game game = new Game();
		game.frame.setTitle(game.title);
		game.frame.add(game);
		game.frame.pack();
		game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		game.frame.setLocationRelativeTo(null);
		game.frame.setVisible(true);
		game.start();
	}
}